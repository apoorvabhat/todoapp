import React, { Component } from 'react'
import { View, Text, Image,TouchableHighlight, TextInput, StyleSheet } from 'react-native'

export default class Inputs extends Component {
   state = {
      email: '',
      password: ''
   }
   handleEmail = (text) => {
      this.setState({ email: text })
   }
  
   handlePassword = (text) => {
      this.setState({ password: text })
   }
   
   login = (email, pass) => {
      alert('email: ' + email + ' password: ' + pass)
   }

   render(){
      return (
         <View style = {styles.container}>
            <Image style = {{height: 100, width: 100}}
            source = {require('./images/mFine-1.png')} />
            
            <TextInput style = {styles.input}
               underlineColorAndroid = "transparent"
               placeholder = "Email"
               placeholderTextColor = "#9a73ef"
               autoCapitalize = "none"
               onChangeText = {this.handleEmail}/>
            
            <TextInput style = {styles.input}
               underlineColorAndroid = "transparent"
               placeholder = "Password"
               placeholderTextColor = "#9a73ef"
                
               onChangeText = {this.handlePassword}/>
               
            <TouchableHighlight
               style = {styles.submitButton}
               onPress = {
                  () => this.login(this.state.email, this.state.password)
               }>
               <Text style = {styles.submitButtonText}> Submit </Text>
            </TouchableHighlight>
            
          
         </View>
      )
   }
}
const styles = StyleSheet.create({
   container: {
      paddingTop: 20
   },
   input: {
      margin: 15,
      height: 40,
      borderColor: 'darkblue',
      borderWidth: 1
   },
   submitButton: {
      backgroundColor: 'darkblue',
      padding: 10,
      margin: 15,
      height: 40,
   },
   submitButtonText:{
      color: 'white'
   }
})