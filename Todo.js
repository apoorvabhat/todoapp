export class Todo extends React.Component {
  state = {
    names: [
       {'name': 'Ben', 'id': 1},
       {'name': 'Susan', 'id': 2},
       {'name': 'Robert', 'id': 3},
       {'name': 'Mary', 'id': 4},
       {'name': 'Daniel', 'id': 5},
       {'name': 'Laura', 'id': 6},

    ]
 }
 render() {
    return (
       <View>
          <ScrollView
          style={{
            flex: 1,
            backgroundColor: 'red'
          }}>
             {
                this.state.names.map((item, index) => {
                  return  (
                   <View key = {index}>
                      <Text>{item.name}</Text>
                   </View>
                  )
                })
             }
          </ScrollView>
       </View>
    )
 }
  
}

//origial code
import React from 'react';
import { StyleSheet, TextInput,Text, View } from 'react-native';

export default class App extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <Text>Open up App.js to start  working on your app!</Text>
        <View>
          
          </View>
      </View>  
    );
  }
}
export class TodoScroll extends React.Component {
    /*
    render() {

    }*/
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
